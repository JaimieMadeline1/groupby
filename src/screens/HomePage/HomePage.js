import React from "react";
import styles from "./HomePage.styl";
import axios from "axios";
import GlobalSpinner from "../../components/Global/GlobalSpinner";

class Product extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className={styles.productTile}>
        <div className={styles.productTileInnerContainer}>
          <section>
            <img src="http://via.placeholder.com/144x180" alt="144 x 180 image" />
            {this.props.sale && (
              <div className={styles.onSaleContainer}>
                <div className={styles.onSaleIcon}>On Sale</div>
              </div>
            )}
          </section>
          <section className={styles.productItemInfoSection}>
            <article>
              <h5 className={styles.productTileTitle}>{this.props.title}</h5>
              <article className={styles.productTilePrice}>
                <p>{this.props.price}</p>
              </article>
            </article>
            <p className={styles.productTileDesc}>{this.props.desc}</p>
          </section>
        </div>
      </div>
    );
  }
}

class Nav extends React.PureComponent {
  constructor(props) {
    super(props);
    // this.state = {
    //   isSelected: props.isSelected == true ? true : false
    // };
  }
  changeSortOrder = () => {
    this.props.changeSortOrder(this.props.attr);
  };
  render() {
    return (
      <li className={styles.navLiContainer} onClick={this.changeSortOrder} value={this.props.attr}>
        <div className={styles.customMarkerLiContainer}>
          <div className={[styles.customMarkerLi, this.props.isSelected ? styles.liSelected : {}].join(" ")}>
            <div className={[styles.customMarkerLiInside, this.props.isSelected ? styles.liSelected : {}].join(" ")} />
          </div>
        </div>
        <span className={styles.navText}>{this.props.label}</span>
      </li>
    );
  }
}

class Grid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      navLabels: this.props.navLabels,
      products: this.props.products,
      sortValue: ""
    };

    if (this.props.navLabels && this.props.navLabels.length > 0) {
      this.state.sortValue = this.props.navLabels[0].attr;
      this.sortItems(this.state.sortValue, true);
    }
  }
  componentDidMount() {
    this.sortItems(this.state.sortValue);
  }
  sortItems(sortValue, firstCall = false) {
    let newProductsOrder = this.state.products;
    if (sortValue == "title") {
      newProductsOrder.sort((a, b) => {
        return a.title > b.title ? 1 : -1;
      });
    }
    if (sortValue == "price") {
      newProductsOrder.sort((a, b) => {
        return a.price - b.price;
      });
    }
    if (firstCall) {
      this.state.products = newProductsOrder;
    } else {
      this.setState({ products: newProductsOrder });
    }
  }
  changeSortOrder = value => {
    if (value != this.state.sortValue) {
      this.setState({ sortValue: value }, () => {
        this.sortItems(value);
      });
    }
  };
  renderNavs = () => {
    return (
      <React.Fragment>
        {this.state.navLabels.map((item, index) => {
          return (
            <Nav
              key={index}
              isSelected={this.state.sortValue == item.attr}
              changeSortOrder={this.changeSortOrder}
              label={item.label}
              attr={item.attr}
            />
          );
        })}
      </React.Fragment>
    );
  };
  renderProducts = () => {
    return (
      <React.Fragment>
        {this.state.products.map((item, index) => {
          return <Product key={index} title={item.title} desc={item.desc} price={item.price} sale={item.sale} />;
        })}
      </React.Fragment>
    );
  };
  render() {
    return (
      <React.Fragment>
        <aside className={styles.nav}>{this.state.navLabels.length > 0 && <ul>{this.renderNavs()}</ul>}</aside>
        <aside className={styles.products}>{this.renderProducts()}</aside>
      </React.Fragment>
    );
  }
}

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPageReady: false
    };

    this.navLabels = [];
    this.products = [];
  }
  componentDidMount() {
    const urlProduct = "https://private-152094-gbitest.apiary-mock.com/products";
    const urlNav = "https://private-152094-gbitest.apiary-mock.com/navs";
    Promise.all([this.getProducts(urlProduct), this.getNavLabels(urlNav)]).then(res => {
      this.setState({ isPageReady: true });
    });
  }
  getProducts(url) {
    return axios.get(url).then(res => {
      this.products = res.data;
    });
  }
  getNavLabels(url) {
    return axios.get(url).then(res => {
      this.navLabels = res.data;
    });
  }
  render() {
    return (
      <React.Fragment>
        <div className={styles.container}>
          {this.state.isPageReady == true ? (
            <div className={styles.wrapper}>
              <Grid navLabels={this.navLabels} products={this.products} />
            </div>
          ) : (
            <div>
              <GlobalSpinner showSpinner={true} />
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default HomePage;
