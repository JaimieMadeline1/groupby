# GroupByTest
Link to startkit: https://bitbucket.org/JaimieMadeline1/react-start-kit-vm/src/master/ 
Main Landing Page is within src/screens/HomePage/HomePage.js

Keywords: Stylus, CSS Modules, Webpack, Redux, Routing, Yarn, Babel, React, VSCode library modifications(Manta's Stylus Supremacy and Prettier).

VS Code Libraries to download:
- language-stylus (understanding stylus files)
- Manta's Stylus Supremacy (formatting stylus files)
- Prettier (code formatting)

CMD lines in order to start project:
- yarn install
- yarn start

Deploy:
- In index.html change lines 56 and 57 or src="/bundle.js" into src="/dist/bundle.js"
- yarn build
- yarn deploy

Live Link:
https://groupby-b9a56.firebaseapp.com/

Bitbucket link:
https://bitbucket.org/JaimieMadeline1/groupby/src/master/ 