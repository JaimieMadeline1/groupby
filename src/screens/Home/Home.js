import React, { Component } from "react";
import homeStyles from "./Home.styl";
// Global Styles, imported only once, folder not under css modules, so styles can be used in any file
import globalStyles from "../../styles/globalStyles.styl";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import RightMenu from "../../routing/RightMenu";

class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        <Header />
        <div className={homeStyles.mainStyle}>
          <RightMenu />
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

export default Home;
